import socket

def create_server(host, port):
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((host, port))
    server_socket.listen(1)
    print(f"Server is listening on {host}:{port}")
    return server_socket

def handle_client(client_socket):
    with open("shared_file.txt", "a+") as file:
        # Receive data from the client and write it to the shared file
        data = client_socket.recv(1024).decode()
        file.write(data + "\n")
        print("Data received and written to the file.")

        # Read the shared file and send its content back to the client
        file.seek(0)
        content = file.read()
        client_socket.sendall(content.encode())

def main():
    host = "0.0.0.0"
    port = 12345

    server_socket = create_server(host, port)

    while True:
        client_socket, client_address = server_socket.accept()
        print(f"Connection from {client_address}")
        handle_client(client_socket)
        client_socket.close()

if __name__ == "__main__":
    main()