import socket

def connect_to_server(host, port):
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((host, port))
    return client_socket

def send_and_receive_data(client_socket, data):
    # Send data to the server
    client_socket.sendall(data.encode())

    # Receive and print the shared file content
    content = client_socket.recv(1024).decode()
    print(f"Shared file content:\n{content}")

def main():
    host = "127.0.0.1"  # Or use the server's IP address if on a different device
    port = 12345

    data = input("Enter the data to write to the shared file: ")

    client_socket = connect_to_server(host, port)
    send_and_receive_data(client_socket, data)
    client_socket.close()

if __name__ == "__main__":
    main()